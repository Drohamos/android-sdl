package mips.sdl;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.TextView;

public class SensorsChoice extends AppCompatActivity {

    private SensorManager mSensorManager = null;

    private Sensor mAccelerometer = null;
    private Sensor mGyroscope = null;
    private Sensor mGPS = null;

    // Listener pour l'accéléromètre
    final SensorEventListener mAcceleroListener = new SensorEventListener() {
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            // Que faire en cas de changement de précision ?
            TextView accelero_data = (TextView) findViewById(R.id.accelero_data);
            accelero_data.setText("Changement précision");
        }

        public void onSensorChanged(SensorEvent event) {
            TextView accelero_data = (TextView) findViewById(R.id.accelero_data);
            accelero_data.setText(event.values[0] + " ; " + event.values[1] + " ; " + event.values[2]);
        }
    };

    final LocationListener locationListener = new LocationListener() {
        public void onLocationChanged(Location location) {
            // Called when a new location is found by the network location provider.
            TextView gps_data = (TextView) findViewById(R.id.gps_data);
            gps_data.setText(location.getLatitude() + " ; " + location.getLongitude());
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        public void onProviderEnabled(String provider) {
        }

        public void onProviderDisabled(String provider) {
        }
    };

    @Override
    public final void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        LocationManager mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        String locationProvider = LocationManager.GPS_PROVIDER;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensors_choice);

        // Liste des capteurs
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        // Liste des éléments de l'interface
        TextView accelero_data = (TextView) findViewById(R.id.accelero_data);
        CheckBox accelero_cb = (CheckBox) findViewById(R.id.accelero_cb);
        TextView gyro_data = (TextView) findViewById(R.id.gyro_data);
        CheckBox gyro_cb = (CheckBox) findViewById(R.id.gyro_cb);
        TextView gps_data = (TextView) findViewById(R.id.gps_data);
        CheckBox gps_cb = (CheckBox) findViewById(R.id.gps_cb);

        // Si l'accéléromètre est disponible...
        if (mAccelerometer != null) {
            // On déclenche le listener
            mSensorManager.registerListener(mAcceleroListener, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        } else {
            // On désactive la checkbox et cache les data
            accelero_data.setVisibility(View.GONE);
            accelero_cb.setEnabled(false);
            accelero_cb.setText("Accéléromètre [ND]");
        }

        // Même chose pour le gyroscope
        if (mGyroscope != null) {
        }
        else {
            gyro_data.setVisibility(View.GONE);
            gyro_cb.setEnabled(false);
            gyro_cb.setText("Gyroscope [ND]");
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            gps_data.setText("OK");

            Location lastKnownLocation = mLocationManager.getLastKnownLocation(locationProvider);
            gps_data.setText(lastKnownLocation.getLatitude() + " ; " + lastKnownLocation.getLongitude() + " ; " + lastKnownLocation.getAltitude());

            return;
        }
        else gps_data.setText("NoGPS");
        mLocationManager.requestLocationUpdates(locationProvider, 0, 0, locationListener);

    }

    // Mise en veille des capteurs quand l'application perd le focus
    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(mAcceleroListener, mAccelerometer);
        //mSensorManager.unregisterListener(mSensorEventListener, mGyroscope);
        //mLocationManager.removeUpdates(locationListener);
    }
    //
    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mAcceleroListener, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        //mSensorManager.registerListener(mSensorEventListener, mGyroscope, SensorManager.SENSOR_DELAY_NORMAL);
    }

}